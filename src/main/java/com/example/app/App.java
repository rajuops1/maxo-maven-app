package com.maxo.app;

import com.maxo.dep.Dep;

public class App 
{
    public static void main( String[] args )
    {
        Dep.hello( "GitLab" );
    }
}
